package com.example.study;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.study.models.Book;
import com.example.study.utils.Tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookListActivity extends AppCompatActivity {

    public String category_id;
    public String category_name;

    public TextView tv_hello;
    public ListView tv_list;
    public ArrayList<Map<String,String >> data;

    private Handler handler = new Handler(){
        public void handleMessage(Message msg){
            Log.d( "HANDLER", "this start" );
            SimpleAdapter adapter = new SimpleAdapter(
                    BookListActivity.this,
                    data,
                    R.layout.book_item,
                    new String[]{ "name" },
                    new int[]{R.id.tv_name}
            );
            tv_list.setAdapter( adapter );//为ListView绑定适配器

            // 设置监听
            tv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String book_id = data.get(position).get("id").toString();
                    String book_name = data.get(position).get("name").toString();

                    // 准备进行跳转
                    Intent intent = new Intent( BookListActivity.this, BookDetailActivity.class );
                    // 参数传递
                    Bundle bundle = new Bundle();
                    bundle.putString( "id", book_id );
                    bundle.putString( "name", book_name );
                    intent.putExtras( bundle );
                    // 进行跳转
                    startActivity( intent );

                    /*Log.d( "click", str );
                    Toast.makeText(BookListActivity.this, str, Toast.LENGTH_SHORT).show();*/
                }
            });

            Log.d( "HANDLER", "this end" );
        }
    };

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        category_id = bundle.getString( "id", "" );
        category_name = bundle.getString( "name", "" );

        // 获取数据显示
        data=new ArrayList<Map<String, String>>();
        new BookListActivity.MainThread().start();

        // 获取 ListView
        tv_list=(ListView)findViewById( R.id.tv_list );
    }

    public class MainThread extends Thread{
        @Override
        public void run() {

            // 处理分页
            String book_string = Tool.sendRequest("http://novel.talers.xyz/lists?id=" + category_id + "&page=1&source=001");
            List<Book> categorys = Tool.getBookArrayList( book_string );
            for ( Book category: categorys ){
                Log.d( "CATE", category.name );
                Map<String, String> cate = new HashMap<String, String>();
                cate.put( "id", "" + category.id );
                cate.put( "name", category.name );
                data.add( cate );
            }
            handler.sendEmptyMessage(1);
        }
    }

}

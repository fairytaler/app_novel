package com.example.study.models;

public class Category {
    public int id;
    public int pid;
    public String name;
    public String cover;
    public String book_num;
    public String category_url;
    public String page_url;
    public int page_time;
    public int status;
    public int delete_time;
    public int update_time;
    public int create_time;
}

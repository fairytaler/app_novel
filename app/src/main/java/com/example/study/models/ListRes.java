package com.example.study.models;

import org.json.JSONStringer;

import java.util.Arrays;
import java.util.List;

public class ListRes {
    public int current_page;
    public int from;
    public int last_page;
    public int per_page;
    public int to;
    public int total;
    public String first_page_url;
    public String last_page_url;
    public String next_page_url;
    public String path;
    public String prev_page_url;

    //public String data;
    public List<Book> data;

}

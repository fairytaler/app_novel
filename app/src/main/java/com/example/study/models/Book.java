package com.example.study.models;

public class Book {
    public int id;
    public String author;
    public String name;
    public String desc;
    public String cover;
    public String page_url;
    public int browse;
    public int page_time;
    public int status;
    public int delete_time;
    public int update_time;
    public int create_time;
}

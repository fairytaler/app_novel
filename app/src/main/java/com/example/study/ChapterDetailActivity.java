package com.example.study;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.study.models.Article;
import com.example.study.models.Chapters;
import com.example.study.utils.Tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChapterDetailActivity extends AppCompatActivity {

    public String book_id;
    public String book_name;

    public Article article;
    public String article_id;
    public String article_name;

    public WebView tv_content;

    public ArrayList<Map<String,String >> data;

    private Handler handler = new Handler(){
        public void handleMessage(Message msg){
            //tv_content.setText( article.content );
            tv_content.getSettings().setDefaultTextEncodingName("utf-8");
            tv_content.loadDataWithBaseURL(null,article.content,"text/html","UTF-8",null);
        }
    };

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter_detail);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        article_id = bundle.getString( "id", "" );
        article_name = bundle.getString( "name", "" );
        book_id = bundle.getString( "book_id", "" );

        // 获取数据显示
        data=new ArrayList<Map<String, String>>();
        new ChapterDetailActivity.MainThread().start();

        // 获取 ListView
        tv_content=(WebView)findViewById( R.id.tv_content );
    }

    public class MainThread extends Thread{
        @Override
        public void run() {
            // 处理分页
            String book_string = Tool.sendRequest("http://novel.talers.xyz/chapterDetail?id=" + article_id + "&book_id="+ book_id +"&source=001");
            article = Tool.getChapterDetail( book_string );
            handler.sendEmptyMessage(1);
        }
    }

}

package com.example.study.adapters;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.study.R;


public class MyWeixinJSON extends BaseAdapter {
    private LayoutInflater mInflater;// 动态布局映射
    private JSONArray list;
    private Context context;
//	private int i = 0;


    public void setList(JSONArray list) {
        this.list = list;
    }

    public MyWeixinJSON(JSONArray list, Context context) {
        this.list = list;
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.length();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        // System.out.println("正在渲染第"+position+"行  +++  "+ i++);
        OneView oneView;
        if (convertView == null) {
            //convertView = mInflater.inflate(R.layout.item_weixin, null);// 根据布局文件实例化view
            oneView = new OneView();
            oneView.title = (TextView) convertView.findViewById(R.id.title);// 找某个控件
            oneView.time = (TextView) convertView.findViewById(R.id.time);
            oneView.info = (TextView) convertView.findViewById(R.id.info);
            //oneView.img = (ImageView) convertView.findViewById(R.id.img);
            convertView.setTag(oneView);// 把View和某个对象关联起来
        } else {
            oneView = (OneView) convertView.getTag();
        }
        JSONObject jObject = null;
        try {
            jObject = list.getJSONObject(position);// 根据position获取集合类中某行数据
            oneView.title.setText(jObject.get("itemID").toString());// 给该控件设置数据(数据从集合类中来)
            oneView.time.setText(jObject.get("price").toString());
            oneView.info.setText(jObject.get("title").toString());
            //oneView.img.setBackgroundResource(R.drawable.special_spring_head2);
            // oneView.img.setImageDrawable(Drawable.createFromStream(new
            // URI(jObject.getString("pic_url"))., "src"));
        } catch (Exception e) {
            // TODO: handle exception
        }
        return convertView;
    }

    /** 把每行布局文件的各个控件包装成一个对象 */
    private class OneView {
        TextView title;
        TextView time;
        TextView info;
        ImageView img;
    }

}

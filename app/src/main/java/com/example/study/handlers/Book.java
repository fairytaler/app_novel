package com.example.study.handlers;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.study.BookListActivity;
import com.example.study.R;

import java.util.ArrayList;
import java.util.Map;

public class Book {

    public ArrayList<Map<String,String >> data;

    public BookListActivity bookListActivity;

    private Handler bookListHandler = new Handler(){
        public void handleMessage(Message msg){
            bookListActivity = new BookListActivity();
            // 获取 ListView
            ListView tv_list=bookListActivity.findViewById( R.id.tv_list );
            Log.d( "HANDLER", "this start" );
            SimpleAdapter adapter = new SimpleAdapter(
                    bookListActivity,
                    data,
                    R.layout.book_item,
                    new String[]{ "name" },
                    new int[]{R.id.tv_name}
            );
            tv_list.setAdapter( adapter );//为ListView绑定适配器
            // 设置监听
            tv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String str = data.get(position).get("id").toString();
                    Log.d( "click", str );
                    Toast.makeText(bookListActivity, str, Toast.LENGTH_SHORT).show();
                }
            });
            Log.d( "HANDLER", "this end" );
        }
    };

}

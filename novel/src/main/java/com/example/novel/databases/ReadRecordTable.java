package com.example.novel.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.novel.models.ReadRecord;

import java.util.ArrayList;

public class ReadRecordTable extends SQLiteOpenHelper {

    public static final String TABLE_NAME = "read_record";
    private static final String DB_NAME = "novel.db";
    private static final int DB_VERSION = 1;

    private static ReadRecordTable mTable = null;
    public SQLiteDatabase mDB = null;

    private ReadRecordTable(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }
    private ReadRecordTable(Context context, int version){
        super(context, DB_NAME, null, version);
    }
    // 表实例
    public static ReadRecordTable getInstance( Context context, int version ){
        if( version > 0 && mTable == null ){
            mTable = new ReadRecordTable( context, version );
        }else if( mTable == null ){
            mTable = new ReadRecordTable( context );
        }
        return mTable;
    }
    // 打开读链接
    public SQLiteDatabase openReadLink(){
        if( mDB == null || !mDB.isOpen() ){
            mDB = mTable.getReadableDatabase();
        }
        return mDB;
    }
    // 打开写链接
    public SQLiteDatabase openWriteLink(){
        if( mDB == null || !mDB.isOpen() ){
            mDB = mTable.getWritableDatabase();
        }
        return mDB;
    }
    // 关闭链接
    public void closeLink(){
        if( mDB == null || !mDB.isOpen() ){
            mDB.close();
            mDB = null;
        }
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String drop_sql = "DROP TABLE IF EXISTS "+ TABLE_NAME +";";
        db.execSQL( drop_sql );
        String create_sql = "CREATE TABLE IF NOT EXISTS "+ TABLE_NAME +" ("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + "book_id INTEGER NOT NULL,"
                + "article_id INTEGER NOT NULL,"
                + "book_name VARCHAR NOT NULL);";
        db.execSQL( create_sql );
    }

    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion ){}

    public int delete( String cond ){
        // this is delete
        return mDB.delete(TABLE_NAME, cond, null);
    }

    /**
     * 查询数据库
     * @param cond
     * @return
     */
    public ArrayList<ReadRecord> query(String cond ){
        String sql = String.format( "select rowid,id,book_id,article_id,book_name from %s where %s;", TABLE_NAME, cond );
        ArrayList<ReadRecord> readRecords = new ArrayList<ReadRecord>();
        Cursor cursor = mDB.rawQuery( sql, null );
        while( cursor.moveToNext() ){
            ReadRecord readRecord = new ReadRecord();
            readRecord.rowid = cursor.getLong(0);
            readRecord.id = cursor.getInt(1);
            readRecord.article_id = cursor.getInt(2);
            readRecord.book_name = cursor.getString(3);
            readRecords.add( readRecord );
        }
        cursor.close();
        return  readRecords;
    }

    /**
     * 进行插入
     * @param readRecord
     * @return
     */
    public long insert(ReadRecord readRecord){
        ContentValues contentValues = new ContentValues();
        contentValues.put( "book_id", readRecord.book_id );
        contentValues.put( "article_id", readRecord.article_id );
        contentValues.put( "book_name", readRecord.book_name );
        return mDB.insert( TABLE_NAME, "",contentValues );
        //return 1;
    }

    /**
     *  进行更新
     * @param readRecord
     * @param cond  String.format( "book_id=%t", book_id )
     * @return
     */
    public int update(ReadRecord readRecord, String cond){

        ContentValues contentValues = new ContentValues();
        contentValues.put( "book_id", readRecord.book_id );
        contentValues.put( "article_id", readRecord.article_id );
        contentValues.put( "book_name", readRecord.book_name );
        return mDB.update( TABLE_NAME, contentValues, cond, null );
        //return 1;
    }

}

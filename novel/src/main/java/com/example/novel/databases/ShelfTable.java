package com.example.novel.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.novel.models.Shelf;

import java.util.ArrayList;

public class ShelfTable extends SQLiteOpenHelper {

    public static final String TABLE_NAME = "shelf";
    private static final String DB_NAME = "novel.db";
    private static final int DB_VERSION = 1;

    private static ShelfTable mTable = null;
    public SQLiteDatabase mDB = null;

    private ShelfTable(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }
    private ShelfTable(Context context, int version){
        super(context, DB_NAME, null, version);
    }
    // 表实例
    public static ShelfTable getInstance( Context context, int version ){
        if( version > 0 && mTable == null ){
            mTable = new ShelfTable( context, version );
        }else if( mTable == null ){
            mTable = new ShelfTable( context );
        }
        return mTable;
    }
    // 打开读链接
    public SQLiteDatabase openReadLink(){
        if( mDB == null || !mDB.isOpen() ){
            mDB = mTable.getReadableDatabase();
        }
        return mDB;
    }
    // 打开写链接
    public SQLiteDatabase openWriteLink(){
        if( mDB == null || !mDB.isOpen() ){
            mDB = mTable.getWritableDatabase();
        }
        return mDB;
    }
    // 关闭链接
    public void closeLink(){
        if( mDB == null || !mDB.isOpen() ){
            mDB.close();
            mDB = null;
        }
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String drop_sql = "DROP TABLE IF EXISTS "+ TABLE_NAME +";";
        db.execSQL( drop_sql );
        String create_sql = "CREATE TABLE IF NOT EXISTS "+ TABLE_NAME +" ("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + "book_id INTEGER NOT NULL,"
                + "article_id INTEGER NOT NULL,"
                + "book_name VARCHAR NOT NULL);";
        db.execSQL( create_sql );
    }

    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion ){}

    public int delete( String cond ){
        // this is delete
        return mDB.delete(TABLE_NAME, cond, null);
    }

    /**
     * 查询数据库
     * @param cond
     * @return
     */
    public ArrayList<Shelf> query( String cond ){
        Log.d( "REQUEST", "cond2 = " + cond );
        String sql = String.format( "select rowid,id,book_id,article_id,book_name from %s where %s;", TABLE_NAME, cond );
        ArrayList<Shelf> shelfs = new ArrayList<Shelf>();
        Cursor cursor = mDB.rawQuery( sql, null );
        while( cursor.moveToNext() ){
            Shelf shelf = new Shelf();
            shelf.rowid = cursor.getLong(0);
            shelf.id = cursor.getInt(1);
            shelf.book_id = cursor.getInt(2);
            shelf.article_id = cursor.getInt(3);
            shelf.book_name = cursor.getString(4);
            shelfs.add( shelf );
        }
        cursor.close();
        return  shelfs;
    }

    public Shelf first( String cond ){
        Log.d( "REQUEST", "cond1 = " + cond );
        Shelf shelf = null;
        ArrayList<Shelf> shelfs = query( cond );
        if( ! shelfs.isEmpty() ){
            shelf = shelfs.get(0);
        }
        return shelf;
    }

    /**
     * 查询数据库
     * @return
     */
    public ArrayList<Shelf> get(){
        String sql = String.format( "select rowid,id,book_id,article_id,book_name from %s;", TABLE_NAME );
        ArrayList<Shelf> shelfs = new ArrayList<Shelf>();
        Cursor cursor = mDB.rawQuery( sql, null );
        while( cursor.moveToNext() ){
            Shelf shelf = new Shelf();
            shelf.rowid = cursor.getLong(0);
            shelf.id = cursor.getInt(1);
            shelf.book_id = cursor.getInt(2);
            shelf.article_id = cursor.getInt(3);
            shelf.book_name = cursor.getString(4);
            shelfs.add( shelf );
        }
        cursor.close();
        return  shelfs;
    }

    /**
     * 进行插入
     * @param shelf
     * @return
     */
    public long insert(Shelf shelf){
        ContentValues contentValues = new ContentValues();
        contentValues.put( "book_id", shelf.book_id );
        contentValues.put( "article_id", shelf.article_id );
        contentValues.put( "book_name", shelf.book_name );
        return mDB.insert( TABLE_NAME, "",contentValues );
        //return 1;
    }

    /**
     *  进行更新
     * @param shelf
     * @param cond  String.format( "book_id=%t", book_id )
     * @return
     */
    public int update(Shelf shelf, String cond){

        ContentValues contentValues = new ContentValues();
        contentValues.put( "book_id", shelf.book_id );
        contentValues.put( "article_id", shelf.article_id );
        contentValues.put( "book_name", shelf.book_name );
        return mDB.update( TABLE_NAME, contentValues, cond, null );
        //return 1;
    }

}

package com.example.novel.service;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.novel.R;
import com.example.novel.control.BookListActivity;
import com.example.novel.control.CategoryActivity;
import com.example.novel.models.Category;
import com.example.novel.utils.Tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryService {

    public ArrayList<Map<String,String>> categorys;
    public ListView tv_list;

    public Handler categoryHandle = new Handler(){
        public void handleMessage(Message msg) {
            Log.d("HANDLER", "this start");
            SimpleAdapter adapter = new SimpleAdapter(
                    new CategoryActivity(),
                    categorys,
                    R.layout.item,
                    new String[]{"name"},
                    new int[]{R.id.tv_name}
            );

            tv_list = new CategoryActivity().findViewById( R.id.tv_list );
            tv_list.setAdapter(adapter);//为ListView绑定适配器

            // 设置监听
            tv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String category_id = categorys.get(position).get("id").toString();
                    String category_name = categorys.get(position).get("name").toString();
                    Log.d("click", "category id=" + category_id);
                    // 准备进行跳转
                    Intent intent = new Intent(new CategoryActivity(), BookListActivity.class);
                    // 参数传递
                    Bundle bundle = new Bundle();
                    bundle.putString("id", category_id);
                    bundle.putString("name", category_name);
                    intent.putExtras(bundle);
                    // 进行跳转
                    new CategoryActivity().startActivity(intent);
                    // 处理返回值
                    //startActivityForResult( intent, 0 );
                    //Toast.makeText(MainActivity.this, str, Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    public class CategoryThread extends Thread{
        @Override
        public void run() {
            categorys = new ArrayList<Map<String, String>>();
            // 处理分页
            String category_string = Tool.sendRequest("http://novel.talers.xyz/category?source=001");
            List<Category> categorys_res = Tool.getCategoryArrayList( category_string );
            for ( Category category: categorys_res ){
                Log.d( "CATE", category.name );
                Map<String, String> cate = new HashMap<String, String>();
                cate.put( "id", "" + category.id );
                cate.put( "name", category.name );
                categorys.add( cate );
            }
            //categoryHandle.sendEmptyMessage(1);
        }
    }



}

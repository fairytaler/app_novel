package com.example.novel.models;

import java.util.List;

public class Chapters {
    public int current_page;
    public int from;
    public int last_page;
    public int per_page;
    public int to;
    public int total;
    public String first_page_url;
    public String last_page_url;
    public String next_page_url;
    public String path;
    public String prev_page_url;

    //public String data;
    public List<Article> data;
}

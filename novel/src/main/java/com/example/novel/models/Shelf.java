package com.example.novel.models;

public class Shelf {
    public long rowid;
    public int id;
    public int book_id;
    public int article_id;
    public String name;
    public String book_name;
    public Article new_article;
}

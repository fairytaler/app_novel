package com.example.novel.models;

public class Article {
    public int id;
    public int bid;
    public int prev_id;
    public int next_id;
    public String chapter;
    public String name;
    public String content;
    public int browse;
    public int sort;
    public int status;
    public int delete_time;
    public int update_time;
    public int create_time;
}

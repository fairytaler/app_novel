package com.example.novel.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent ) {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
        if( mNetworkInfo == null ){
            Toast.makeText( context, "无可用网络..." , Toast.LENGTH_LONG).show();
        }else{
            Log.d( "netState", mNetworkInfo.getTypeName() + mNetworkInfo.isConnected() );
            if( mNetworkInfo.isConnected() ){
                //
                if( mNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE ){
                    Toast.makeText( context, "当前使用移动数据..." , Toast.LENGTH_LONG).show();
                }else if( mNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI ){
                    Toast.makeText( context, "当前使用wifi..." , Toast.LENGTH_LONG).show();
                }
            }
        }
        /*if (mNetworkInfo != null) {
            Log.d( "netState", mNetworkInfo.getTypeName() + mNetworkInfo.isConnected() );
            return mNetworkInfo.isAvailable();
        }
        return false;*/
    }
}

package com.example.novel.utils;

import android.util.Log;
import android.util.MalformedJsonException;

import com.example.novel.databases.ShelfTable;
import com.example.novel.models.Article;
import com.example.novel.models.Book;
import com.example.novel.models.BookDetail;
import com.example.novel.models.Category;
import com.example.novel.models.Chapters;
import com.example.novel.models.ListRes;
import com.example.novel.models.Shelf;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Tool {

    private ShelfTable shelfTable;

    private static String novel_domain = "http://novel.talers.xyz/";
    private static String shelf_url = "love";
    private static String search_url = "search";

    public static String sendRequest( String path ){
        Log.d( "REQUEST",  "url: " + path );
        Log.d( "REQUEST",  "start " );
        try {
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5000);
            conn.setDoInput(true);
            conn.connect();
            int responseCode = conn.getResponseCode();
            Log.d( "REQUEST",  responseCode + "" );
            if (responseCode == 200) {
                // 5,得到服务器返回数据
                InputStream inputStream = conn.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    response.append(line);
                }
                String resultString = response.toString();
                Log.d( "REQUEST", resultString );
                return resultString;
            }
        } catch (MalformedJsonException e) {
            Log.d( "REQUEST",  "error Malf " + e.getMessage() );
            e.printStackTrace();
        } catch (IOException e) {
            Log.d( "REQUEST", "error IO " + e.getMessage() );
            e.printStackTrace();
        }
        return null;
    }

    //public static ArrayList<Map<String,String>> strToArrayList( String str ){
    public static List<Category> getCategoryArrayList(String str ){
        List<Category> categorys = new Gson().fromJson( str, new TypeToken<List<Category>>(){}.getType());
        return categorys;
    }

    public static List<Book> getBookArrayList( String str ){
        Log.d( "REQUEST", "book_res = " + str );
        // 获取书籍结果
        ListRes listRes = new Gson().fromJson( str, new TypeToken<ListRes>(){}.getType() );
        List<Book> books = listRes.data;
        // 获取书籍列表
        for (Book book : books) {
            System.out.println("Value = " + book.id);
        }
        return books;
    }

    public static BookDetail getBookDetail( String str){
        Log.d( "REQUEST", "book_detail_res = " + str );
        // 获取书籍结果
        BookDetail detailRes = new Gson().fromJson( str, BookDetail.class );
        return detailRes;
    }

    public static Chapters getChapters( String str ){
        Log.d( "REQUEST", "chapters_res = " + str );
        // 获取书籍结果
        Chapters detailRes = new Gson().fromJson( str, Chapters.class );
        return detailRes;
    }

    public static Article getChapterDetail(String str ){
        Log.d( "REQUEST", "chapter_detail_res = " + str );
        // 获取书籍结果
        Article article = new Gson().fromJson( str, Article.class );
        return article;
    }

    public static List<Shelf> getShelfs( String ids ){
        // http://novel.talers.xyz/love?ids=82,14553,21,14552
        String path = novel_domain + shelf_url + "?ids=" + ids;
        String str = sendRequest( path );
        Log.d( "REQUEST", "shelf_res = " + str );
        List<Shelf> shelfs = new Gson().fromJson( str, new TypeToken<List<Shelf>>(){}.getType());
        return shelfs;
    }

    public static List<Book> getSearshs( String searchText ){
        // http://novel.talers.xyz/love?ids=82,14553,21,14552
        String path = novel_domain + search_url + "?search=" + searchText;
        String str = sendRequest( path );
        Log.d( "REQUEST", "search_res = " + str );
        List<Book> searchs = new Gson().fromJson( str, new TypeToken<List<Book>>(){}.getType());
        return searchs;
    }

    /**
     * 检查网络连接
     * @return
     */
    /*public static boolean isNetworkConnected() {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
        if (mNetworkInfo != null) {
            Log.d( "netState", mNetworkInfo.getTypeName() + mNetworkInfo.isConnected() );
            return mNetworkInfo.isAvailable();
        }
        return false;
    }*/
}

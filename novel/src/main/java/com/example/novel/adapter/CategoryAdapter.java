package com.example.novel.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.example.novel.R;
import com.example.novel.models.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements MyItemClickListener{
    private Context mContext;
    public ArrayList<Map<String,String>> categorys;

    private MyItemClickListener myItemClickListener;


    public CategoryAdapter( Context context, ArrayList<Map<String,String>> publicCategorys ){
        mContext = context;
        categorys = publicCategorys;
    }

    public int getItemCount(){
        return categorys.size();
    }

    public ViewHolder onCreateViewHolder(ViewGroup vg, int viewType){
        View v = LayoutInflater.from(mContext).inflate(R.layout.category_item, vg, false );
        return new ItemHolder( v );
    }

    public void onBindViewHolder(ViewHolder vh, final int position){
        ItemHolder holder = (ItemHolder) vh;
        //holder.iv_image.setImageResource( categorys.get(position).cover );
        holder.tv_name.setText( categorys.get(position).get("name") );
        //
        /*holder.ll_item.setOnClickListener( new OnClickListener(){
            public void onClick( View v ){
                if( mOnItemClickListener != null ){
                    mOnItemClickListener.onItemClick( v, position );
                }
            }
        } );*/
    }

    public int getItemViewType(int position){
        return 0;
    }

    public long getItemId( int position ){
        return position;
    }

    // 定义列表项，视图持有者
    public class ItemHolder extends RecyclerView.ViewHolder{

        public LinearLayout ll_item;
        public ImageView iv_image;
        public TextView tv_name;

        public ItemHolder(View v){
            super(v);
            ll_item = v.findViewById(R.id.ll_item);
            //iv_image = v.findViewById( R.id.iv_image );
            tv_name = v.findViewById( R.id.tv_name );
            v.setOnClickListener( new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    if( myItemClickListener != null ){
                        myItemClickListener.onItemClick( v, getPosition() );
                    }
                }
            } );
        }
    }

    // 声明列表项点击监听
    private AdapterView.OnItemClickListener mOnItemClickListener;
    public void setOnItemClickListener(AdapterView.OnItemClickListener listener){
        this.mOnItemClickListener=listener;
    }


    // 声明列表项长按监听
    private AdapterView.OnItemLongClickListener mOnItemLongClickListener;
    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener listener){
        this.mOnItemLongClickListener=listener;
    }

    // 处理列表项点击事件
    public void onItemClick( View view, int position ){
        String desc = String.format( "click name = %s", position + "" );
        Toast.makeText(mContext, desc, Toast.LENGTH_SHORT).show();
    }

    // 处理列表项长按点击事件
    /*public void onItemLongClick( View view, int position ){
        String desc = String.format( "long click name = %s", categorys.get(position).name );
        Toast.makeText(mContext, desc, Toast.LENGTH_SHORT).show();
    }*/

    // 声明列表项点击监听
    public void setOnItemClickListener(MyItemClickListener listener){
        myItemClickListener=listener;
    }


}

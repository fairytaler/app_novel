package com.example.novel.adapter;

import android.view.View;

public interface MyItemClickListener {
    public void onItemClick(View view, int position );
}

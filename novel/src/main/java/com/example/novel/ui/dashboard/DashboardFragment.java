package com.example.novel.ui.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.novel.MainActivity;
import com.example.novel.R;
import com.example.novel.control.BookDetailActivity;
import com.example.novel.control.ChapterDetailActivity;
import com.example.novel.models.Book;
import com.example.novel.models.Shelf;
import com.example.novel.utils.Tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DashboardFragment extends Fragment {

    public ArrayList<Map<String,String>> books;
    public String searchInput;


    public EditText searchText;
    public Button searchBtn;
    public ListView searchList;

    private DashboardViewModel dashboardViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        /*final TextView textView = root.findViewById(R.id.text_dashboard);
        dashboardViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        searchText = root.findViewById(R.id.search_text);
        searchBtn = root.findViewById(R.id.search_btn);
        searchList = root.findViewById(R.id.search_list);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchInput = searchText.getText().toString();
                // 定义
                books = new ArrayList<Map<String, String>>();
                // 发送请求
                new DashboardFragment.MainThread().start();

                //Toast.makeText( getActivity(), "文本是："+ searchInput, Toast.LENGTH_SHORT).show();
            }
        });

        return root;
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            Log.d("HANDLER", "this start");
            SimpleAdapter adapter = new SimpleAdapter(
                    getContext(),
                    books,
                    R.layout.search_item,
                    new String[]{"book_name"},
                    new int[]{R.id.tv_name}
            );
            searchList.setAdapter(adapter);//为ListView绑定适配器
            // 设置监听
            searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String book_id = books.get(position).get("book_id").toString();
                    String book_name = books.get(position).get("book_name").toString();
                    Log.d("click", "book id=" + book_id);
                    // 准备进行跳转
                    Intent intent = new Intent(getActivity(), BookDetailActivity.class);
                    // 参数传递
                    Bundle bundle = new Bundle();
                    bundle.putString("id", book_id);
                    bundle.putString("name", book_name);
                    intent.putExtras(bundle);

                    // 进行跳转
                    startActivity(intent);
                    // 处理返回值
                    //startActivityForResult( intent, 0 );
                    //Toast.makeText(MainActivity.this, str, Toast.LENGTH_SHORT).show();
                }
            });

            Log.d("HANDLER", "this end");
        }
    };

    public class MainThread extends Thread{
        @Override
        public void run() {

            List<Book> search_res = Tool.getSearshs( searchInput );
            for ( Book book: search_res ){
                Log.d( "CATE", book.name );
                Map<String, String> cate = new HashMap<String, String>();
                cate.put( "book_id", "" + book.id );
                cate.put( "book_name", book.name );
                books.add( cate );
            }
            handler.sendEmptyMessage(1);
        }
    }




}
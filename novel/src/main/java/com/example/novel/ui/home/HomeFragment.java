package com.example.novel.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.novel.MainActivity;
import com.example.novel.R;
import com.example.novel.adapter.CategoryAdapter;
import com.example.novel.adapter.MyItemClickListener;
import com.example.novel.control.BookListActivity;
import com.example.novel.control.CategoryActivity;
import com.example.novel.models.Category;
import com.example.novel.utils.Tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeFragment extends Fragment implements MyItemClickListener {

    public ArrayList<Map<String,String>> categorys;
    public ArrayList<Category> categorys_res;
    public ListView tv_category_list;

    public View root;
    private HomeViewModel homeViewModel;

    private void initRecyclerLinear(){
        // 获取视图
        /*RecyclerView rv_liner = root.findViewById( R.id.rv_linear );
        //创建 垂直线性管理器
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        // 设置循环视图的布局管理器
        rv_liner.setLayoutManager(manager);
        //rv_liner.addItemDecoration();
        // 构建列表线性适配器
        CategoryAdapter adapter = new CategoryAdapter( getActivity(), categorys);
        adapter.setOnItemClickListener( adapter );
        rv_liner.setAdapter( adapter );*/
    }

    public void onItemClick( View view, int position ){
        Log.d( "REQUEST", "rec is click " + position );
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        root = inflater.inflate(R.layout.fragment_home, container, false);
        /*final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        // 获取数据显示
        categorys=new ArrayList<Map<String, String>>();
        // 获取 ListView
        tv_category_list=root.findViewById( R.id.tv_category_list );
        new HomeFragment.MainThread().start();
        return root;
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {

            /*initRecyclerLinear();
            return;*/

            Log.d("HANDLER", "this start");
            SimpleAdapter adapter = new SimpleAdapter(
                    getActivity(),
                    categorys,
                    R.layout.item,
                    new String[]{"name"},
                    new int[]{R.id.tv_name}
            );
            tv_category_list.setAdapter(adapter);//为ListView绑定适配器
            // 设置监听
            tv_category_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String category_id = categorys.get(position).get("id").toString();
                    String category_name = categorys.get(position).get("name").toString();
                    Log.d("click", "category id=" + category_id);
                    // 准备进行跳转
                    Intent intent = new Intent(getActivity(), BookListActivity.class);
                    // 参数传递
                    Bundle bundle = new Bundle();
                    bundle.putString("id", category_id);
                    bundle.putString("name", category_name);
                    intent.putExtras(bundle);
                    // 进行跳转
                    startActivity(intent);
                    // 处理返回值
                    //startActivityForResult( intent, 0 );
                    //Toast.makeText(MainActivity.this, str, Toast.LENGTH_SHORT).show();
                }
            });

            Log.d("HANDLER", "this end");
        }
    };

    public class MainThread extends Thread{
        @Override
        public void run() {

            // 处理分页
            String category_string = Tool.sendRequest("http://novel.talers.xyz/category?source=001");
            List<Category> categorys_res = Tool.getCategoryArrayList( category_string );
            for ( Category category: categorys_res ){
                Log.d( "CATE", category.name );
                Map<String, String> cate = new HashMap<String, String>();
                cate.put( "id", "" + category.id );
                cate.put( "name", category.name );
                categorys.add( cate );
            }
            handler.sendEmptyMessage(1);
        }
    }

}
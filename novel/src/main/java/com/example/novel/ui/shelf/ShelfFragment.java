package com.example.novel.ui.shelf;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.novel.R;
import com.example.novel.control.BookDetailActivity;
import com.example.novel.control.BookListActivity;
import com.example.novel.databases.ShelfTable;
import com.example.novel.models.Category;
import com.example.novel.models.Shelf;
import com.example.novel.utils.Tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShelfFragment extends Fragment {

    private ShelfTable shelfTable;

    private ShelfViewModel shelfViewModel;


    public ArrayList<Map<String,String>> shelfs;
    public String ids = "";


    public ListView tv_shelf_list;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        shelfViewModel =
                ViewModelProviders.of(this).get(ShelfViewModel.class);
        View root = inflater.inflate(R.layout.fragment_shelf, container, false);
        /*final TextView textView = root.findViewById(R.id.tv_shelf_list);
        shelfViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        // 获取数据显示
        shelfs=new ArrayList<Map<String, String>>();
        // 获取 ListView
        tv_shelf_list=root.findViewById( R.id.tv_shelf_list );

        // 创建数据库链接
        shelfTable = ShelfTable.getInstance( getActivity() , 3 );
        // 开始查询
        shelfTable.openReadLink();
        ArrayList<Shelf> shelfTables = shelfTable.get();
        shelfTable.closeLink();
        if( ! shelfTables.isEmpty() ){
            for ( Shelf shelf:shelfTables  ){
                ids = ids + "," + shelf.book_id;
            }
        }
        // 发送请求
        new ShelfFragment.MainThread().start();

        return root;
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            Log.d("HANDLER", "this start");
            SimpleAdapter adapter = new SimpleAdapter(
                    getActivity(),
                    shelfs,
                    R.layout.shelf_item,
                    new String[]{"book_info"},
                    new int[]{R.id.tv_name}
            );
            tv_shelf_list.setAdapter(adapter);//为ListView绑定适配器
            // 设置监听
            tv_shelf_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String book_id = shelfs.get(position).get("book_id").toString();
                    String book_name = shelfs.get(position).get("book_name").toString();
                    Log.d("click", "book id=" + book_id);
                    // 准备进行跳转
                    Intent intent = new Intent(getActivity(), BookDetailActivity.class);
                    // 参数传递
                    Bundle bundle = new Bundle();
                    bundle.putString("id", book_id);
                    bundle.putString("name", book_name);
                    intent.putExtras(bundle);

                    // 进行跳转
                    startActivity(intent);
                    // 处理返回值
                    //startActivityForResult( intent, 0 );
                    //Toast.makeText(MainActivity.this, str, Toast.LENGTH_SHORT).show();
                }
            });

            Log.d("HANDLER", "this end");
        }
    };

    public class MainThread extends Thread{
        @Override
        public void run() {

            // 处理分页
            List<Shelf> shelfs_res = Tool.getShelfs( ids );
            for ( Shelf shelf: shelfs_res ){
                Log.d( "CATE", shelf.name );
                Map<String, String> cate = new HashMap<String, String>();
                cate.put( "book_id", "" + shelf.id );
                cate.put( "book_name", shelf.name );
                cate.put( "book_info", shelf.name + "\n" + shelf.new_article.name );
                cate.put( "article_name", shelf.new_article.name );
                shelfs.add( cate );
            }
            handler.sendEmptyMessage(1);
        }
    }

}
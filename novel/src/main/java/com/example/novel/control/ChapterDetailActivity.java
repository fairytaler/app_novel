package com.example.novel.control;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.novel.MainActivity;
import com.example.novel.R;
import com.example.novel.databases.ShelfTable;
import com.example.novel.models.Article;
import com.example.novel.models.Shelf;
import com.example.novel.utils.CustomScrollView;
import com.example.novel.utils.Tool;

import java.util.ArrayList;
import java.util.Map;

public class ChapterDetailActivity extends AppCompatActivity implements CustomScrollView.OnScrollChangeListener {

    public ShelfTable shelfTable;

    public String book_id;
    public String book_name;

    public String one;
    public String two;
    public String three;

    public Article article;
    public String article_id;
    public String article_name;
    public String prev_id = "0";
    public String next_id = "0";

    public TextView tv_content;
    public TextView tv_article_name;
    public TextView tv_prev;
    public TextView tv_chapters;
    public TextView tv_shelf;
    public TextView tv_next;
    public CustomScrollView sv_box;

    public ArrayList<Map<String,String >> data;

    private Handler handler = new Handler(){
        public void handleMessage(Message msg){
            record();
            //tv_content.setMovementMethod(ScrollingMovementMethod.getInstance());
            sv_box.scrollTo(0,0);
            setTitle(article_name);
            tv_content.setText( Html.fromHtml( article.content ) );
        }
    };

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter_detail);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        article_id = bundle.getString( "id", "" );
        article_name = bundle.getString( "name", "" );
        book_id = bundle.getString( "book_id", "" );

        // 创建数据库链接
        shelfTable = ShelfTable.getInstance( this , 3 );

        // 获取数据显示
        data=new ArrayList<Map<String, String>>();
        new ChapterDetailActivity.MainThread().start();

        // 获取 ListView
        tv_content=findViewById( R.id.tv_content );

        tv_prev=findViewById( R.id.tv_prev );
        //tv_chapters=findViewById( R.id.tv_chapters );
        tv_shelf=findViewById( R.id.tv_shelf );
        tv_next=findViewById( R.id.tv_next );

        sv_box=findViewById( R.id.sv_box );

        sv_box.setOnScrollChangeListener(this);

        tv_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( Integer.valueOf( prev_id ) <= 0 ){
                    Toast.makeText(ChapterDetailActivity.this, "已经是第一章了", Toast.LENGTH_SHORT).show();
                    return;
                }
                article_id = prev_id;
                new ChapterDetailActivity.MainThread().start();
                /*// 准备进行跳转
                Intent intent = new Intent( ChapterDetailActivity.this, ChapterDetailActivity.class );
                // 参数传递
                Bundle bundle = new Bundle();
                bundle.putString( "id", prev_id );
                bundle.putString( "book_id", book_id );
                bundle.putString( "name", "" );
                intent.putExtras( bundle );
                // 进行跳转
                startActivity( intent );*/
            }
        });

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( Integer.valueOf( next_id) <= 0 ){
                    Toast.makeText(ChapterDetailActivity.this, "已经是最后一章了", Toast.LENGTH_SHORT).show();
                    return;
                }
                article_id = next_id;
                new ChapterDetailActivity.MainThread().start();

                // 准备进行跳转
                /*Intent intent = new Intent( ChapterDetailActivity.this, ChapterDetailActivity.class );
                // 参数传递
                Bundle bundle = new Bundle();
                bundle.putString( "id", next_id );
                bundle.putString( "book_id", book_id );
                bundle.putString( "name", "" );
                intent.putExtras( bundle );
                // 进行跳转
                startActivity( intent );*/
            }
        });

        tv_shelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 准备进行跳转
                Intent intent = new Intent( ChapterDetailActivity.this, MainActivity.class );
                // 进行跳转
                startActivity( intent );
            }
        });

    }

    public class MainThread extends Thread{
        @Override
        public void run() {
            // 处理分页
            String book_string = Tool.sendRequest("http://novel.talers.xyz/chapterDetail?id=" + article_id + "&book_id="+ book_id +"&source=001");
            article = Tool.getChapterDetail( book_string );
            article_name = article.name;
            prev_id = article.prev_id + "";
            next_id = article.next_id + "";
            handler.sendEmptyMessage(1);
        }
    }

    // 添加阅读记录
    protected void record()
    {
        // 检测是否已经加入书架， 加入书架则记录阅读进度
        // 开始查询
        shelfTable.openReadLink();
        Shelf shelf = shelfTable.first( "book_id=" + book_id  );
        shelfTable.closeLink();
        if( shelf != null ){
            shelfTable.openWriteLink();
            shelf.article_id = Integer.valueOf( article_id );
            int res = shelfTable.update( shelf, "book_id=" + book_id );
            shelfTable.closeLink();
            //Toast.makeText(ChapterDetailActivity.this, "res = " + res + " aid= " + article_id, Toast.LENGTH_SHORT).show();
        }
    }




    @Override
    public void onScrollToStart() {

        return ;

        /*if( Integer.valueOf( prev_id ) <= 0 ){
            return;
        }
        article_id = prev_id;
        new ChapterDetailActivity.MainThread().start();*/

        //Toast.makeText(this, "滑动到顶部了", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onScrollToEnd() {

        if( Integer.valueOf( next_id) <= 0 ){
            Toast.makeText(ChapterDetailActivity.this, "已经是最后一章了", Toast.LENGTH_SHORT).show();
            return;
        }
        article_id = next_id;
        new ChapterDetailActivity.MainThread().start();

        //Toast.makeText(this, "滑动到底部了", Toast.LENGTH_SHORT).show();
    }


}

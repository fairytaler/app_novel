package com.example.novel.control;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.novel.R;
import com.example.novel.databases.ShelfTable;
import com.example.novel.models.Article;
import com.example.novel.models.Book;
import com.example.novel.models.BookDetail;
import com.example.novel.models.ReadRecord;
import com.example.novel.models.Shelf;
import com.example.novel.utils.Tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class BookDetailActivity extends AppCompatActivity {

    private ShelfTable shelfTable;

    public String book_id;
    public String book_name;
    public String book_desc;
    public Book book;
    public Article article;
    public ArrayList<Map<String,String >> data;
    public ArrayList<Map<String,String >> chapters;

    public TextView tv_book_name;
    public TextView tv_book_desc;
    public TextView tv_select_chapters;
    public TextView tv_select_record;
    public TextView tv_add_shelf;
    public ListView tv_list;

    private Handler handler = new Handler(){
        public void handleMessage(Message msg){
            Log.d( "HANDLER", "this start" );

            tv_book_name.setText( book_name );
            tv_book_desc.setText(Html.fromHtml( book_desc ) );
            tv_book_desc.setMovementMethod(ScrollingMovementMethod.getInstance());

            shelfTable.openReadLink();
            ArrayList<Shelf> shelfs = shelfTable.query( "book_id=" + book_id );
            shelfTable.closeLink();
            if( shelfs.isEmpty() ){
                tv_add_shelf.setText( "加入收藏" );
            }else{
                Log.d( "REQUEST", "shelf size = " + shelfs.size() );
                Log.d( "REQUEST", "shelf book_id = " + shelfs.get(0).book_id );
                Log.d( "REQUEST", "shelf book_name = " + shelfs.get(0).book_name );
                Log.d( "REQUEST", "shelf article_id = " + shelfs.get(0).article_id );
                tv_add_shelf.setText( "取消收藏" );
            }

            SimpleAdapter adapter = new SimpleAdapter(
                    BookDetailActivity.this,
                    data,
                    R.layout.book_detail_item,
                    new String[]{ "name" },
                    new int[]{R.id.tv_name}
            );
            tv_list.setAdapter( adapter );//为ListView绑定适配器

            // 设置监听
            tv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String article_id = data.get(position).get("id").toString();
                    String article_name = data.get(position).get("name").toString();

                    // 准备进行跳转
                    Intent intent = new Intent( BookDetailActivity.this, ChapterDetailActivity.class );
                    // 参数传递
                    Bundle bundle = new Bundle();
                    bundle.putString( "id", article_id );
                    bundle.putString( "book_id", book_id );
                    bundle.putString( "name", article_name );
                    intent.putExtras( bundle );
                    // 进行跳转
                    startActivity( intent );

                    /*Log.d( "click", str );
                    Toast.makeText(BookListActivity.this, str, Toast.LENGTH_SHORT).show();*/
                }
            });

            Log.d( "HANDLER", "this end" );
        }
    };

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        book_id = bundle.getString( "id", "" );
        book_name = bundle.getString( "name", "" );

        // 获取数据显示
        data=new ArrayList<Map<String, String>>();
        new BookDetailActivity.MainThread().start();

        // 获取 ListView
        tv_book_name=findViewById( R.id.tv_book_name );
        tv_book_desc=findViewById( R.id.tv_book_desc );
        tv_select_chapters=findViewById( R.id.tv_select_chapters );
        tv_select_record=findViewById( R.id.tv_select_record );
        tv_add_shelf=findViewById( R.id.tv_add_shelf );
        tv_list=(ListView)findViewById( R.id.tv_list );

        tv_select_chapters.setOnClickListener(new ChaptersClick());
        tv_select_record.setOnClickListener(new RecordClick());
        tv_add_shelf.setOnClickListener(new ShelfClick());

        shelfTable = ShelfTable.getInstance( this, 3 );

    }
    // 跳转目录列表
    class ChaptersClick implements View.OnClickListener{
        public void onClick(View v){
            // 准备进行跳转
            Intent intent = new Intent( BookDetailActivity.this, ChaptersActivity.class );
            // 参数传递
            Bundle bundle = new Bundle();
            bundle.putString( "id", book_id );
            bundle.putString( "name", book_name );
            intent.putExtras( bundle );
            // 进行跳转
            startActivity( intent );
        }
    }
    // 跳转上次阅读
    class RecordClick implements View.OnClickListener{
        public void onClick(View v){

            // 开始查询
            shelfTable.openReadLink();
            Shelf shelf = shelfTable.first( "book_id=" + book_id );
            shelfTable.closeLink();
            if( shelf == null ){
                Toast.makeText(BookDetailActivity.this, "请加入书架", Toast.LENGTH_SHORT).show();
                return;
            }

            if( shelf.article_id == 0 ){
                Toast.makeText(BookDetailActivity.this, "无阅读记录", Toast.LENGTH_SHORT).show();
                return;
            }

            // 准备进行跳转
            Intent intent = new Intent( BookDetailActivity.this, ChapterDetailActivity.class );
            // 参数传递
            Bundle bundle = new Bundle();
            bundle.putString( "id", String.valueOf( shelf.article_id ) );
            bundle.putString( "book_id", String.valueOf( shelf.book_id ) );
            bundle.putString( "name", "" );
            intent.putExtras( bundle );
            // 进行跳转
            startActivity( intent );
        }
    }

    // 跳转上次阅读
    class ShelfClick implements View.OnClickListener{
        public void onClick(View v){

            shelfTable.openReadLink();
            ArrayList<Shelf> shelfs = shelfTable.query( "book_id=" + book_id );
            shelfTable.closeLink();
            shelfTable.openWriteLink();
            if( shelfs.isEmpty() ){
                Shelf shelf = new Shelf();
                shelf.book_id = Integer.valueOf(book_id);
                shelf.article_id = 0;
                shelf.book_name = book_name;
                shelfTable.insert( shelf );
                tv_add_shelf.setText( "取消收藏" );
                String str = "加入书架成功";
                Toast.makeText(BookDetailActivity.this, str, Toast.LENGTH_SHORT).show();
            }else{

                AlertDialog a=new AlertDialog.Builder(BookDetailActivity.this)
                        .setTitle("")
                        .setMessage("是否确定取消")
                        .setPositiveButton("是",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                //TODO
                                tv_add_shelf.setText( "加入收藏" );
                                shelfTable.delete( "book_id=" + book_id  );
                            }
                        } ).setNegativeButton("否", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                //TODO
                            }
                        }).create();
                a.show();

            }
            shelfTable.closeLink();
        }
    }

    public class MainThread extends Thread{
        @Override
        public void run() {
            // 处理分页
            String book_string = Tool.sendRequest("http://novel.talers.xyz/detail?id=" + book_id + "&source=001");
            BookDetail detailRes = Tool.getBookDetail( book_string );
            book = detailRes.book;
            book_name = detailRes.book.name;
            book_desc = detailRes.book.desc;
            for ( Article article: detailRes.articles ){
                Log.d( "CATE", article.name );
                Map<String, String> cate = new HashMap<String, String>();
                cate.put( "id", "" + article.id );
                cate.put( "name", article.name );
                data.add( cate );
            }
            handler.sendEmptyMessage(1);
        }
    }

}

package com.example.novel.control;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.novel.MainActivity;
import com.example.novel.R;
import com.example.novel.models.Article;
import com.example.novel.models.Chapters;
import com.example.novel.utils.Tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChaptersActivity extends AppCompatActivity {

    public String book_id;
    public String book_name;
    public int page = 1;
    public Chapters chapters;

    public TextView tv_hello;
    public ListView tv_list;
    public TextView tv_prev;
    public TextView tv_shelf;
    public TextView tv_next;

    public ArrayList<Map<String,String >> data;

    private Handler handler = new Handler(){
        public void handleMessage(Message msg){
            Log.d( "HANDLER", "this start" );
            SimpleAdapter adapter = new SimpleAdapter(
                    ChaptersActivity.this,
                    data,
                    R.layout.chapters_item,
                    new String[]{ "name" },
                    new int[]{R.id.tv_name}
            );
            tv_list.setAdapter( adapter );//为ListView绑定适配器

            // 设置监听
            tv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String article_id = data.get(position).get("id").toString();
                    String article_name = data.get(position).get("name").toString();

                    // 准备进行跳转
                    Intent intent = new Intent( ChaptersActivity.this, ChapterDetailActivity.class );
                    // 参数传递
                    Bundle bundle = new Bundle();
                    bundle.putString( "id", article_id );
                    bundle.putString( "book_id", book_id );
                    bundle.putString( "name", article_name );
                    intent.putExtras( bundle );
                    // 进行跳转
                    startActivity( intent );

                    /*Log.d( "click", str );
                    Toast.makeText(BookListActivity.this, str, Toast.LENGTH_SHORT).show();*/
                }
            });

            Log.d( "HANDLER", "this end" );
        }
    };

    @Override
    @SuppressWarnings("unchecked")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapters);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        book_id = bundle.getString( "id", "" );
        book_name = bundle.getString( "name", "" );
        page = bundle.getInt( "page", 1 );

        // 获取数据显示
        new ChaptersActivity.MainThread().start();

        // 获取 ListView
        tv_list=(ListView)findViewById( R.id.tv_list );

        tv_prev=findViewById( R.id.tv_prev );
        tv_shelf=findViewById( R.id.tv_shelf );
        tv_next=findViewById( R.id.tv_next );

        tv_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( page <= 1 ){
                    return;
                }
                // 获取数据显示
                page = page - 1;
                new ChaptersActivity.MainThread().start();

                /*// 准备进行跳转
                Intent intent = new Intent( ChaptersActivity.this, ChaptersActivity.class );
                // 参数传递
                Bundle bundle = new Bundle();
                bundle.putInt( "page", page - 1 );
                bundle.putString( "id", book_id );
                bundle.putString( "name", book_name );
                intent.putExtras( bundle );
                // 进行跳转
                startActivity( intent );*/
            }
        });

        tv_shelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 准备进行跳转
                Intent intent = new Intent( ChaptersActivity.this, MainActivity.class );
                // 进行跳转
                startActivity( intent );
            }
        });

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( page >= chapters.total ){
                    return;
                }
                // 获取数据显示
                page = page + 1;
                new ChaptersActivity.MainThread().start();

                /*// 准备进行跳转
                Intent intent = new Intent( ChaptersActivity.this, ChaptersActivity.class );
                Bundle bundle = new Bundle();
                bundle.putInt( "page", page + 1 );
                bundle.putString( "id", book_id );
                bundle.putString( "name", book_name );
                intent.putExtras( bundle );
                // 进行跳转
                startActivity( intent );*/
            }
        });

    }

    public class MainThread extends Thread{
        @Override
        public void run() {
            data=new ArrayList<Map<String, String>>();
            // 处理分页
            String book_string = Tool.sendRequest("http://novel.talers.xyz/chapters?id=" + book_id + "&page="+ page +"&source=001");
            chapters = Tool.getChapters( book_string );

            for ( Article category: chapters.data ){
                Log.d( "CATE", category.name );
                Map<String, String> cate = new HashMap<String, String>();
                cate.put( "id", "" + category.id );
                cate.put( "name", category.name );
                data.add( cate );
            }
            handler.sendEmptyMessage(1);
        }
    }

}
